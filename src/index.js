import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


const posts = [
  {
    title: "Soviet soldiers playing a piano in the ruins of Berlin (1945)", 
    picture_url: "https://i.redd.it/88l1x2srs0g21.jpg",
    tag: "Картинка",
    subreddit: "r/russia",
    author: "u/chasegrambling123",
    timestamp: "9 hours ago",
  }
]

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: Array(15).fill(posts[0]),
      isLoadingPosts: false,
    };

    window.onscroll = () => {
      if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight)
        this.loadMorePosts();
    }
  }

  async loadMorePosts(post_count=10) {
    if (this.state.isLoadingPosts)
      return;
    this.setState({isLoadingPosts: true});
    await new Promise(r => setTimeout(r, 400));
    this.setState({
      // TODO: pick random elements from global const posts
      posts: this.state.posts.concat(Array(post_count).fill(posts[0])),
      isLoadingPosts: false,
    });
    console.log("New posts loaded");
  }

  render() {
    return (
      <div className="app">
        <div className="navbar"><h2>Little Reddit</h2></div>
        <div className="content">
          <ol className="post-list">
            {this.state.posts.map((post) => 
              <Post
                title={post.title}
                picture_url={post.picture_url}
                tag={post.tag}
                subreddit={post.subreddit}
                author={post.author}
                timestamp={post.timestamp}
              />              
            )}
          </ol>
        </div>
        <div className="load-more-wrapper">
          <button className="load-more-button" onClick={() => this.loadMorePosts()}>Load more</button>
        </div>
      </div>
    )
  }
}

class Post extends React.Component {
  render() {
    const post_picture_style = this.props.picture_url && {
      backgroundImage: 'url(' + (this.props.thumbnail_url || this.props.picture_url) + ')'
    };
    return (

      <li className="post">
        {this.props.picture_url &&
          <div className="post-picture" style={post_picture_style}>
          </div>
        }
        <span className="post-title">{this.props.title}</span>
        {this.props.picture_url && 
          <span className="post-image-link"><a href={this.props.picture_url}>{this.props.picture_url}</a></span>
        }
        {this.props.tag && 
          <div className="post-tags">
            <span className="post-tag">{this.props.tag}</span>
          </div>
        }
        <div className="post-source">
          <span className="post-source-subreddit">{this.props.subreddit}</span>
          <span className="post-source-author">Posted by {this.props.author}</span>
          <span className="post-source-timestamp">{this.props.timestamp}</span>
        </div>
        <div className="post-reactions">
          <div className="post-reactions-votes"></div>
          <div className="post-reactions-comments"></div>
          <button className="post-reactions-more">...</button>
        </div>
      </li>          
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
